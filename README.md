# README #

Demo Registration and Discovery 1 Service's accompanying source code for blog entry at http://tech.asimio.net/2016/11/14/Microservices-Registration-and-Discovery-using-Spring-Cloud-Eureka-Ribbon-and-Feign.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Docker host or Docker machine
* Running Eureka cluster

### Building and executing the application from command line ###

```
mvn clean package
java -DappPort=8501 -DhostName=$HOSTNAME -Deureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" -jar target/demo-registration-api-1.jar
```

Eureka cluster Open http://localhost:8001 or http://localhost:8002 in a browser

### How do I get set up using Docker? ###

```
sudo docker pull asimio/demo-registration-api-1

Multiple containers:
sudo docker run -idt -p 8501:8501 --net=host -e appPort=8501 -e hostName=$HOSTNAME -e eureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" asimio/demo-registration-api-1:1.0.25
sudo docker run -idt -p 8502:8502 --net=host -e appPort=8502 -e hostName=$HOSTNAME -e eureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" asimio/demo-registration-api-1:1.0.25
```

Open http://localhost:8001 or http://localhost:8002 in a browser

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero