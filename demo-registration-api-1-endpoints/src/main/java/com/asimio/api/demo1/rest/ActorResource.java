package com.asimio.api.demo1.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.asimio.api.demo1.model.Actor;

@Component
@Path("/actors")
@Produces(MediaType.APPLICATION_JSON)
public class ActorResource {

	@GET
	public List<Actor> findActors() {
		List<Actor> result = new ArrayList<>();
		result.add(this.buildActor("1", "First1", "Last1"));
		result.add(this.buildActor("2", "First2", "Last2"));
		return result;
	}

	@GET
	@Path("{id}")
	public Actor getActor(@PathParam("id") String id) {
		return this.buildActor(id, String.format("First%s", id), String.format("Last%s", id));
	}

	private Actor buildActor(String id, String firstName, String lastName) {
		Actor result = new Actor();
		result.setActorId(id);
		result.setFirstName(firstName);
		result.setLastName(lastName);
		return result;
	}
}